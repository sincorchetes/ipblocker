#!/bin/bash
#
# IP-Blocker
# Written by Álvaro Castillo
# @sincorchetes
# https://echemosunbitstazo.es
# GPLv2
#
#
#----------------DIRECTORIES-------------------------------------------#

ip_blocked_log_dir=/admin/logs/ipblocker
ip_blocked_list_tmp=${ip_blocked_log_dir}/ip_list.tmp
ip_blocked_list=${ip_blocked_log_dir}/ip_list.$(date +%d-%m-%Y).txt

# Next version: ip_blocked_log=${ip_blocked_log_dir}/blocked_ip.$(date +%d-%m-%Y).log
#----------------------------------------------------------------------#

# Check if logs directory was created first.

if [ ! -d ${ip_blocked_log_dir} ]
then

  # If It does not exist, create. If created, skip.

  mkdir -p ${ip_blocked_log_dir}
fi 

# Check if list blocked was created.

if [ ! -f ${ip_blocked_list} ]
then

  # Creates if It does not exists.

  touch ${ip_blocked_list}

fi

# That's check this IP main list is created or not.

if [ -f ${ip_blocked_list} ]
then

  # Get IP failed connection over SSH 

  ip_blocker_arg=$(cat /var/log/audit/audit.log |grep failed | awk '{print $11}' | cut -f 2 -d "=")

  # Redirect info to new temporaly file

  echo -e "${ip_blocker_arg}" > "${ip_blocked_list_tmp}"

  # Store IP list without duplicates IPs

  ip_nodu=$(sort ${ip_blocked_list_tmp} | uniq)
  echo -e "${ip_nodu}" > ${ip_blocked_list_tmp}
  
  # Get value of index in both files.

  ip_tmp_index=$(wc ${ip_blocked_list_tmp} -l | awk '{print $1}')
  ip_list_index=$(wc ${ip_blocked_list} -l | awk '{print $1}')

  # First validation, if IP main list is empty, that's copy IP of tmp list.

  if [ ${ip_list_index} -eq 0 ]
      then
        cat ${ip_blocked_list_tmp} >> ${ip_blocked_list}

	# Send data into firewall to reject bad IP 

        list_to_apply=($(cat ${ip_blocked_list}))
        for ip_block in ${list_to_apply}
          do
            firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='${ip_block}' reject"
          done

	# Reloading firewall applying new changes

        firewall-cmd --reload

	# If process does work well, that's nice.

        if [ $? -eq 0 ]
        then
          echo "Firewall reloaded successfully! 2"

        else
          echo "Firewall cannot reloaded. Please check logs journalctl -xe"
        fi
        echo "List was updated"

  # Second validation, if IP tmp list is more grant than IP main list.

  elif [ "${ip_tmp_index}" -gt "${ip_list_index}" ]
    then

      # Store the value of index to compare.

      ip_compare_tmp_list=($(cat ${ip_blocked_list_tmp}))
      ip_compare_list=($(cat ${ip_blocked_list}))
       
      for ip_tmp in ${ip_compare_tmp_list[@]}
        do
          for ip in ${ip_compare_list[@]}
            do

            # Compare between arrays

              if [ "${ip}" == "${ip_tmp}" ]
                then
                   
                  # In new version will be sent to log

                  echo "IP registered ${ip_tmp}"
              else

                  # Add in main IP list

                  echo -e "${ip_tmpp_blocked_listip_blocked_list}" >> ${ip_blocked_list} 
              fi
            done
        done

        # Load new file at once with news IP added without any duplicate IP

        sanitize_list=$(sort ${ip_blocked_list} | uniq)
        echo -e "${sanitize_list}" > ${ip_blocked_list}

        # Starting applying changes into firewall

        list_to_apply=($(cat ${ip_blocked_list}))
        for ip_block in ${list_to_apply}
          do
            firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='${ip_block}' reject"
          done
        firewall-cmd --reload
        if [ $? -eq 0 ]
        then
          echo "Firewall reloaded successfully! 1"
        else
          echo "Firewall cannot reloaded. Please check logs journalctl -xe"
        fi

  # Third validation, if IP main list is more gran or equal than IP tmp list, compare both list and get differences to apply.

  elif [ ${ip_list_index} -ge ${ip_tmp_index} ]
    then
      ip_compare_tmp_list=($(cat ${ip_blocked_list_tmp}))
      ip_compare_list=($(cat ${ip_blocked_list}))

      for ip in ${ip_compare_list[@]}
        do
          for ip_tmp in ${ip_compare_tmp_list[@]}
            do
              if [ ${ip_tmp} == ${ip} ]
                then
                  echo "IP registered ${ip_tmp}"
              else
                  echo -e "${ip_tmp}" >> ${ip_blocked_list}
              fi
            done
        done

        # Same workflow as before statements.

        sanitize_list=$(sort ${ip_blocked_list} | uniq)
        echo -e "${sanitize_list}" > ${ip_blocked_list}

        list_to_apply=($(cat ${ip_blocked_list}))
        for ip_block in ${list_to_apply}
          do
            firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='${ip_block}' reject"
          done
        firewall-cmd --reload
        if [ $? -eq 0 ]
        then
          echo "Firewall reloaded successfully! 3 "
        else
          echo "Firewall cannot reloaded. Please check logs journalctl -xe"
        fi

  # Get tmp IP index, if does not have news IP, stop script.

  elif [ ${ip_tmp_index} -eq 0 ]
  then
    echo "No new IPs found"
  else
      echo "List not updated."
  fi
else
  echo "Something was wrong"
fi

# Delete temporary file
rm ${ip_blocked_list_tmp}
