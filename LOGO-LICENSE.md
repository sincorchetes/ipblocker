This image is designed by Numix Circle icon theme pack.
You can find here:
https://github.com/numixproject/numix-icon-theme-circle

Note: You can remove this icon if you want to use GPLv2.
