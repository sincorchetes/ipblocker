# IP Blocker

STOP crap IP that's tries access into your computer without legal access.

Note: This script it is not adapted to use zones in FirewallD, that's feature to build in the future.

# Requirements
- FirewallD installed and active
- AWK installed

# How to works?

Clone this repository, give `+x` perm to script and run as root.

You can get more information in comments of this script.

# Workflow

1. Read audit file in `/var/log/audit/audit.log`
2. Extract of it IP with failed access
3. Redirect into new tmp file
4. Compare with main IP file
5. Apply rule to block that's IP found (firewalld)
6. Delete tmp IP file

# Cron?

Yes, you can add into your system crontab to save your server of dangerous IP :D

# Next versions
- Save IPs blocked into log
- Sent a log via email
- Improve script workflow
- Etc..
